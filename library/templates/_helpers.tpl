{{/*
Default Template for Deployment. All Sub-Charts under this Chart can include the below template.
*/}}
{{- define "library.deploymentConfigTpl" }}
apiVersion: apps.openshift.io/v1
kind: DeploymentConfig
{{ template "library.metadata" . }}
spec:
  replicas: {{ .replicas }}
  selector:
    app: {{ .app }}
    template: {{ .tpl }}
{{- end }}


{{/*
Default Template for Service. All Sub-Charts under this Chart can include the below template.
*/}}
{{- define "library.serviceTpl" }}
apiVersion: v1
kind: Service
{{ template "library.metadata" . }}
spec:
  selector:
    app: {{ .app  }}
{{- end }}

{{/*
Default Template for ConfigMap. All Sub-Charts under this Chart can include the below template.
*/}}
{{- define "library.configMapTpl" }}
apiVersion: v1
kind: ConfigMap
{{ template "library.namedMetadata" . }}
data:
{{- end }}

{{/*
Default Template for Route. All Sub-Charts under this Chart can include the below template.
*/}}
{{- define "library.routeTpl" }}
apiVersion: route.openshift.io/v1
kind: Route
{{ template "library.metadata" . }}
  annotations:
    router.cern.ch/network-visibility: {{ .visibility }}
  {{- if (eq .visibility "Internet") }}
    haproxy.router.openshift.io/ip_whitelist: ''
  {{- end }}
status:
  ingress: []
spec:
  tls:
    insecureEdgeTerminationPolicy: Redirect
    termination: edge
  to:
    kind: Service
    name: {{ .app }}
    weight: 100
{{- end }}

{{/*
Default Template for Metadata. All Sub-Charts under this Chart can include the below template.
*/}}
{{- define "library.metadata" }}
metadata:
  name: {{ .app  }}
  labels:
    app: {{ .app  }}
    template: {{ .tpl }}
{{- end }}

{{/*
Default Template for Metadata with custom names. All Sub-Charts under this Chart can include the below template.
*/}}
{{- define "library.namedMetadata" }}
metadata:
  name: {{ .name }}
  labels:
    app: {{ .app }}
    template: {{ .tpl }}
{{- end }}

{{/*
Default Template for Secrets. All Sub-Charts under this Chart can include the below template.
*/}}
{{- define "library.secretTpl" }}
apiVersion: v1
kind: Secret
{{ template "library.namedMetadata" . }}
type: Opaque
data:
  {{ .key }}: {{ .value | b64enc }}
{{- end }}

{{/*
Default Template for ConfigMap. All Sub-Charts under this Chart can include the below template.
*/}}
{{- define "library.horizontalPodAutoscalerTpl" }}
apiVersion: autoscaling/v2
kind: HorizontalPodAutoscaler
{{ template "library.metadata" . }}
spec:
  maxReplicas: {{ .maxReplicas }}
  minReplicas: {{ .minReplicas }}
  scaleTargetRef:
    apiVersion: apps.openshift.io/v1
    kind: DeploymentConfig
    name: {{ .app }}

{{- end }}